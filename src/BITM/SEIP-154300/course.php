<?php

namespace App;


class course
{
    private $banglaMark;
    private $englishMark;
    private $mathMark;

    private $banglaGrade;
    private $englishGrade;
    private $mathGrade;


    public function setBanglaMark($banglaMark)
    {
        $this->banglaMark = $banglaMark;
    }

    public function setBanglaGrade()
    {
        $this->banglaGrade = $this->convertMark2Grade($this->banglaMark);
    }


    public function setEnglishMark($englishMark)
    {
        $this->englishMark = $englishMark;
    }

    public function setEnglishGrade()
    {
        $this->englishGrade = $this->convertMark2Grade($this->englishMark);
    }

    public function setMathMark($mathMark)
    {
        $this->mathMark = $mathMark;
    }

    public function setMathGrade()
    {
        $this->mathGrade = $this->convertMark2Grade($this->mathMark);
    }


    public function getBanglaMark()
    {
        return $this->banglaMark;
    }

    public function getBanglaGrade()
    {
        return $this->banglaGrade;
    }


    public function getEnglishMark()
    {
        return $this->englishMark;
    }

    public function getEnglishGrade()
    {
        return $this->englishGrade;
    }


    public function getMathMark()
    {
        return $this->mathMark;
    }

    public function getMathGrade()
    {
        return $this->mathGrade;
    }





    public function convertMark2Grade($mark){


        switch($mark){


            case ($mark>79): return "A+"; break;
            case ($mark>74): return "A"; break;
            case ($mark>69): return "A-"; break;
            case ($mark>64): return "B+"; break;
            case ($mark>59): return "B"; break;
            case ($mark>54): return "B-"; break;
            case ($mark>49): return "C+"; break;
            case ($mark>44): return "C"; break;
            case ($mark>39): return "D"; break;
            default: return "A+";

        }
    }


}