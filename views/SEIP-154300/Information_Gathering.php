<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="stylesheet" href="../../resources/style.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/style.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <title>Students Information and Marks Input Form</title>

</head>
<body>

<form action="process.php" method="post">


    <div class="container">
        <div class="form-group">
            <label for="name" >Student's Name</label>
            <input type="text" class="form-control" name="name" placeholder="Enter Your Name..">
            <label for="name">Student's ID</label>
            <input type="text" class="form-control" name="studentID" placeholder="Type Student's ID Here...">
    </div>

        <div class="form-group">
            <label for="name">Bangla Mark:</label>
            <input type="number" step="any" class="form-control" name="banglaMark" placeholder="Type Bangla Mark Here...">

            <label for="name">English Mark:</label>
            <input type="number" step="any" class="form-control" name="englishMark" placeholder="Type English Mark Here...">

            <label for="name">Math Mark:</label>
            <input type="number" step="any" class="form-control" name="mathMark" placeholder="Type Mathematics Mark Here...">

        </div>


        <button type="submit" class="btn btn-primary btn-lg" >Submit</button>



</div>



</form>

</body>
</html>