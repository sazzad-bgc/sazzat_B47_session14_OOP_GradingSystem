<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="stylesheet" href="../../resources/style.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/style.css">
    <link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <title>Students Information and Marks Input Form</title>
</head>

<body>

<?php
require_once("../../vendor/autoload.php");

//var_dump($_POST);
$objStudent = new \App\Student();

$objStudent->setName($_POST["name"]);
$objStudent->setStudentID($_POST["studentID"]);


$name = $objStudent->getName();
$studentID = $objStudent->getStudentID();

$objCourse = new \App\course();

$objCourse->setBanglaMark($_POST["banglaMark"]);
$objCourse->getBanglaGrade();

$objCourse->setEnglishMark($_POST["englishMark"]);
$objCourse->getEnglishGrade();

$objCourse->setMathMark($_POST["mathMark"]);
$objCourse->getBanglaGrade();

$banglaMark = $objCourse->getBanglaMark();
$englishMark = $objCourse->getEnglishMark();
$mathMark = $objCourse->getMathMark();

$banglaGrade = $objCourse->getBanglaGrade();
$englishGrade = $objCourse->getEnglishGrade();
$mathGrade = $objCourse->getMathGrade();

$resultPage = <<<RESULT


<div class='container'>
<div class='table-responsive'>
<table class ='table'>

<tr>
    <th>Student Name</th>
    <th>Student ID</th>
</tr>
</table>
</div>
</div>

RESULT;

echo $resultPage;
?>

</body>
</html>